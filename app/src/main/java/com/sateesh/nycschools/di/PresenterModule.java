package com.sateesh.nycschools.di;

import com.sateesh.nycschools.ui.detail.SchoolDetailContract;
import com.sateesh.nycschools.ui.detail.SchoolDetailPresenter;
import com.sateesh.nycschools.ui.list.SchoolListContract;
import com.sateesh.nycschools.ui.list.SchoolListPresenter;

import dagger.Binds;
import dagger.Module;

/**
 * @author sateesh devabhaktuni
 */

@Module
public abstract class PresenterModule {

    @Binds
    abstract SchoolListContract.Presenter schoolListPresenter(SchoolListPresenter schoolListPresenter);


    @Binds
    abstract SchoolDetailContract.Presenter schoolDetailPresenter(SchoolDetailPresenter schoolDetailPresenter);
}