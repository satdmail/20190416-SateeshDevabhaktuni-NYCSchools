package com.sateesh.nycschools.ui.detail;

import com.sateesh.nycschools.data.model.SchoolDetailResponse;
import com.sateesh.nycschools.ui.base.BasePresenter;
import com.sateesh.nycschools.ui.base.BaseView;

/**
 * @author sateesh devabhaktuni
 * Outlines the relationship between SchoolDetailContract.View and SchoolDetailContract.Presenter
 */
public interface SchoolDetailContract {
    interface View extends BaseView<Presenter> {
        /**
         * Provides data to the TextViews
         *
         * @param schoolDetailResponse
         */
        void displayData(SchoolDetailResponse schoolDetailResponse);

        /**
         * Shows noDataFoundTextView when SAT query fails
         */
        void noDataFoundTextView();
    }

    interface Presenter extends BasePresenter<View> {
        /**
         * @param dbn:        Unique id for each School to query for SAT records
         * @param schoolName: Name of school in SAT query in case query fails
         * @param unknown:    String resource passed to TextViews in case query fails
         */
        void getSchoolDetails(String dbn, String schoolName, String unknown);

    }
}
