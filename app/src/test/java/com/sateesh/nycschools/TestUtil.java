package com.sateesh.nycschools;


import com.sateesh.nycschools.data.model.SchoolDetailResponse;
import com.sateesh.nycschools.data.model.SchoolListResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sateesh devabhaktuni
 * Test Utility to provide test data
 */
public class TestUtil {

    public static List<SchoolListResponse> testListData() {
        List<SchoolListResponse> schoolListResponses = new ArrayList<>();
        SchoolListResponse firstSchool = new SchoolListResponse();
        firstSchool.setSchoolName("Pan American International High School at Monroe");
        firstSchool.setDbn("12X388");
        SchoolListResponse secondSchool = new SchoolListResponse();
        secondSchool.setSchoolName("World View High School");
        secondSchool.setDbn("10X353");
        SchoolListResponse thirdSchool = new SchoolListResponse();
        thirdSchool.setSchoolName("Pelham Lab High School");
        thirdSchool.setDbn("08X320");
        schoolListResponses.add(firstSchool);
        schoolListResponses.add(secondSchool);

        return schoolListResponses;
    }

    public static List<SchoolDetailResponse> testData() {
        List<SchoolDetailResponse> schoolDetailResponseList = new ArrayList<>();
        SchoolDetailResponse schoolDetailResponse = new SchoolDetailResponse();
        schoolDetailResponse.setDbn("12X388");
        schoolDetailResponse.setSchoolName("Pan American International High School at Monroe");
        schoolDetailResponseList.add(schoolDetailResponse);

        return schoolDetailResponseList;
    }
}