package com.sateesh.nycschools.di;

import com.sateesh.nycschools.BuildConfig;
import com.sateesh.nycschools.data.SchoolRepository;
import com.sateesh.nycschools.data.remote.SchoolService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * @author sateesh devabhaktuni
 */
@Module(includes = PresenterModule.class)
public class AppModule {

    @Provides
    @Singleton
    public SchoolService schoolService() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(SchoolService.class);
    }

    @Provides
    @Singleton
    public SchoolRepository schoolRepository(SchoolService schoolService) {
        return new SchoolRepository(schoolService);
    }
}
