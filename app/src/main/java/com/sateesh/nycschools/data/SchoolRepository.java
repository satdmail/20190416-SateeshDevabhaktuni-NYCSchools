package com.sateesh.nycschools.data;


import com.sateesh.nycschools.data.model.SchoolListResponse;
import com.sateesh.nycschools.data.remote.SchoolService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * @author sateesh devabhaktuni
 * Repository layer for caching data to avoid unnecessary network calls
 */
public class SchoolRepository {

    private final SchoolService schoolService;
    private List<SchoolListResponse> listOfSchools;

    @Inject
    public SchoolRepository(SchoolService schoolService) {
        this.schoolService = schoolService;
        listOfSchools = new ArrayList<>();
    }

    /**
     * Makes a network call if data set is empty otherwise returns data
     *
     * @return listOfSchools: list of school information
     */
    public Single<List<SchoolListResponse>> getListOfSchools() {
        if (listOfSchools.isEmpty()) {
            return schoolService.getSchoolList()
                    .map(schoolListResponses -> {
                        listOfSchools.addAll(schoolListResponses);

                        return listOfSchools;
                    });
        } else {
            return Single.just(listOfSchools);
        }
    }
}