package com.sateesh.nycschools.ui.base;

/**
 * @author sateesh devabhaktuni
 */
public interface BaseView<T> {
    /**
     * Shows network error on loading failure
     */
    void showError();

    /**
     * initialized loading bar
     */
    void startingLoadingBar();

    /**
     * hides loading bar from screen
     */
    void hideLoadingBar();
}
