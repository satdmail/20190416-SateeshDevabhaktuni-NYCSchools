package com.sateesh.nycschools.di;

import com.sateesh.nycschools.ui.detail.SchoolDetailFragment;
import com.sateesh.nycschools.ui.list.SchoolListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author sateesh devabhaktuni
 */
@Module
public abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract SchoolListFragment contributeSchoolListFragment();

    @ContributesAndroidInjector
    abstract SchoolDetailFragment contributeSchoolDetailFragment();
}