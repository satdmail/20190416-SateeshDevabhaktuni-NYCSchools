package com.sateesh.nycschools.di;

import com.sateesh.nycschools.ui.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author sateesh devabhaktuni
 */
@Module
public abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = FragmentsModule.class)
    abstract MainActivity contributeMainActivity();
}